import { Connection } from 'typeorm';
import { User } from '../entity/User';

export class UsersDataManager {

    constructor(private connection: Connection) {}

    public async createUser(user: User): Promise<User> {
        return await this.connection.query(`INSERT INTO user (firstName, lastName) VALUES (${user.firstName}, ${user.lastName})`);
    }

    public async findUserById(id: string): Promise<User> {
        return await this.connection.query(`SELECT * FROM users WHERE id=${id}`);
    }

    public async deleteUserById(id: string): Promise<User> {
        return await this.connection.query(`DELETE * FROM users WHERE id=${id}`);
    }

    public async updateUserById(id: string, user:User): Promise<User> {
        return await this.connection.query(`UPDATE users SET firstName = ${user.firstName}, lastName = ${user.lastName} WHERE id=${id}`);
    }
}