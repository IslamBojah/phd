import { Controller, Get, Param, Post, Body, Delete, Put } from '@nestjs/common';
import { User } from '../entity';
import { UsersDataManager } from '../DataManager/UsersDataManager';

@Controller('users')
export class UsersController {
  constructor(private usersDataManager: UsersDataManager) {}

    
  @Post(':first_name/:last_password')
  public async craeteNewUser(@Param() params: {first_name: string, last_name: string}): Promise<User> {
    const user = new User();
    user.firstName = params.first_name;
    user.lastName = params.last_name;
    return await this.usersDataManager.createUser(user);
  }  

  @Get()
  public async getUser(@Body() body: {id: string}): Promise<User> {
    return await this.usersDataManager.findUserById(body.id);
  }  


  @Delete(':id')
  public async deleteUser(@Param() params: {id: string}): Promise<User> {
    return await this.usersDataManager.deleteUserById(params.id);
  }

  @Put(':id')
  public async updateUser(@Param() params: {id: string}, @Body() body:{first_name: string, last_name: string}): Promise<User> {
    const user = new User();
    user.firstName = body.first_name;
    user.lastName = body.last_name;
    return await this.usersDataManager.updateUserById(params.id, user);
  }

}
