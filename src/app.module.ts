import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersController } from './controller/users.controller';
import { dbConfig } from '../config/database';
import { UsersDataManager } from './DataManager/UsersDataManager';

@Module({
  imports: [dbConfig],
  controllers: [AppController, UsersController],
  providers: [AppService, UsersDataManager],

})

export class AppModule {}