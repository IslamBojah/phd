import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

export interface IUser {
    firstName: string,
    lastName: string,
}


@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: true })
  isActive: boolean;
}

export class UserEntity implements IUser {
    public firstName: string;
    public lastName: string;

    constructor (options) {
        for(const key in options) {
            this[key] = options[key];
        }
    }
}