import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../src/entity';

export const  dbConfig = TypeOrmModule.forRoot({
      type: 'mysql',
      host: '192.168.112.175',
      port: 81,
      username: 'root',
      password: 'usbw',
      database: 'users',
      entities: [User],
      synchronize: true,
    });
  
